#include "totro.hpp"

#include <iostream> 
#include <string>
#include <chrono>

int main(int ac, char**av)
{
    std::string buf;
    int repeats = 1;
    if (ac > 1) {
        buf.resize(std::stoi(av[1]));
    } else {
        buf.resize(8);
    }
    if (ac > 2) {
        repeats = std::stoi(av[2]);
    }
    constexpr uint32_t largest_32bit_prime = 0xFFFFFFFBu;
    uint32_t seed = 
        std::chrono::steady_clock::now().time_since_epoch().count() 
        % largest_32bit_prime; 
    for (int i = 0; i < repeats; ++i) {
        totro::Generate(&buf[0], buf.size(), ++seed);
        buf[0] = std::toupper(buf[0]);
        std::cout << buf << "\n";
    }
    return 0;
}
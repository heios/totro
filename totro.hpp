#pragma once

#include <array>
#include <cstdint>
#include <random>
#include <algorithm>
#include <cstring>
#include <type_traits>

namespace totro {

enum class AllowedPos : int8_t {
    ENDING = 1,
    BEGINING = 1 << 1,
    MIDDLE = 1 << 2,
    EVERYWHERE = 0
        | AllowedPos::ENDING 
        | AllowedPos::BEGINING 
        | AllowedPos::MIDDLE,
    BEG_MID = AllowedPos::BEGINING | AllowedPos::MIDDLE,
    MID_END = AllowedPos::MIDDLE | AllowedPos::ENDING,
};

using AllowedPosUT = std::underlying_type<AllowedPos>::type;
AllowedPosUT operator & (AllowedPos lhs, AllowedPos rhs) {
    return static_cast<AllowedPosUT>(lhs) & static_cast<AllowedPosUT>(rhs);
}

using syllable_t = std::array<char, 4>;  // Null-terminated string.

#if __cplusplus < 201402L
struct SyllableSpec_t
{
    syllable_t syllable;
    AllowedPos allowed_pos;
    int8_t cumulative_weigth;
};
#else
struct SyllableSpec_t
{
    syllable_t syllable{};
    AllowedPos allowed_pos{};
    int8_t cumulative_weigth{};
};
#endif

size_t strlen(const SyllableSpec_t& syll_spec) {
    return ::strlen(syll_spec.syllable.data());
}

constexpr auto vowels = std::array<SyllableSpec_t, 26> {
    SyllableSpec_t{
     {'a'}, AllowedPos::EVERYWHERE, 12
    },
    {{'e'}, AllowedPos::EVERYWHERE, 24},
    {{'i'}, AllowedPos::EVERYWHERE, 36},
    {{'o'}, AllowedPos::EVERYWHERE, 48},
    {{'u'}, AllowedPos::EVERYWHERE, 60},

    {{'a', 'e'}, AllowedPos::EVERYWHERE, 61},
    {{'a', 'i'}, AllowedPos::EVERYWHERE, 62},
    {{'a', 'o'}, AllowedPos::EVERYWHERE, 63},
    {{'a', 'u'}, AllowedPos::EVERYWHERE, 64},
    {{'a', 'a'}, AllowedPos::EVERYWHERE, 65},

    {{'e', 'a'}, AllowedPos::EVERYWHERE, 66},
    {{'e', 'o'}, AllowedPos::EVERYWHERE, 67},
    {{'e', 'u'}, AllowedPos::EVERYWHERE, 68},
    {{'e', 'e'}, AllowedPos::EVERYWHERE, 69},

    {{'i', 'a'}, AllowedPos::EVERYWHERE, 70},
    {{'i', 'o'}, AllowedPos::EVERYWHERE, 71},
    {{'i', 'u'}, AllowedPos::EVERYWHERE, 72},
    {{'i', 'i'}, AllowedPos::EVERYWHERE, 73},

    {{'o', 'a'}, AllowedPos::EVERYWHERE, 74},
    {{'o', 'e'}, AllowedPos::EVERYWHERE, 75},
    {{'o', 'i'}, AllowedPos::EVERYWHERE, 76},
    {{'o', 'u'}, AllowedPos::EVERYWHERE, 77},
    {{'o', 'o'}, AllowedPos::EVERYWHERE, 78},

    {{'e', 'a', 'u'}, AllowedPos::EVERYWHERE, 79},
    {{'\''}, AllowedPos::MIDDLE, 80},
    {{'y'}, AllowedPos::EVERYWHERE, 81},
};


constexpr auto consonants = std::array<SyllableSpec_t, 52> {
    SyllableSpec_t{
     {'b'}, AllowedPos::EVERYWHERE, 3
    },
    {{'c'}, AllowedPos::EVERYWHERE, 6},
    {{'d'}, AllowedPos::EVERYWHERE, 9},
    {{'f'}, AllowedPos::EVERYWHERE, 12},
    {{'g'}, AllowedPos::EVERYWHERE, 15},
    {{'h'}, AllowedPos::EVERYWHERE, 18},
    {{'j'}, AllowedPos::EVERYWHERE, 21},
    {{'k'}, AllowedPos::EVERYWHERE, 24},
    {{'l'}, AllowedPos::EVERYWHERE, 27},
    {{'m'}, AllowedPos::EVERYWHERE, 30},
    {{'n'}, AllowedPos::EVERYWHERE, 33},
    {{'p'}, AllowedPos::EVERYWHERE, 36},
    {{'r'}, AllowedPos::EVERYWHERE, 39},
    {{'s'}, AllowedPos::EVERYWHERE, 42},
    {{'t'}, AllowedPos::EVERYWHERE, 45},
    {{'v'}, AllowedPos::EVERYWHERE, 48},
    {{'w'}, AllowedPos::EVERYWHERE, 51},
    {{'x'}, AllowedPos::EVERYWHERE, 54},
    {{'y'}, AllowedPos::EVERYWHERE, 57},
    {{'z'}, AllowedPos::EVERYWHERE, 60},

    {{'s', 'c'}, AllowedPos::EVERYWHERE, 61},

    {{'c', 'h'}, AllowedPos::EVERYWHERE, 62},
    {{'g', 'h'}, AllowedPos::EVERYWHERE, 63},
    {{'p', 'h'}, AllowedPos::EVERYWHERE, 64},
    {{'s', 'h'}, AllowedPos::EVERYWHERE, 65},
    {{'t', 'h'}, AllowedPos::EVERYWHERE, 66},
    {{'w', 'h'}, AllowedPos::BEG_MID, 67},

    {{'c', 'k'}, AllowedPos::MID_END, 68},
    {{'n', 'k'}, AllowedPos::MID_END, 69},
    {{'r', 'k'}, AllowedPos::MID_END, 70},
    {{'s', 'k'}, AllowedPos::EVERYWHERE, 71},
    {{'w', 'k'}, AllowedPos::MID_END, 72},

    {{'c', 'l'}, AllowedPos::BEG_MID, 73},
    {{'f', 'l'}, AllowedPos::BEG_MID, 74},
    {{'g', 'l'}, AllowedPos::BEG_MID, 75},
    {{'k', 'l'}, AllowedPos::BEG_MID, 76},
    {{'l', 'l'}, AllowedPos::BEG_MID, 77},
    {{'p', 'l'}, AllowedPos::BEG_MID, 78},
    {{'s', 'l'}, AllowedPos::BEG_MID, 79},

    {{'b', 'r'}, AllowedPos::BEG_MID, 81},
    {{'c', 'r'}, AllowedPos::BEG_MID, 82},
    {{'d', 'r'}, AllowedPos::BEG_MID, 84},
    {{'f', 'r'}, AllowedPos::BEG_MID, 86},
    {{'g', 'r'}, AllowedPos::BEG_MID, 88},
    {{'k', 'r'}, AllowedPos::BEG_MID, 90},
    {{'p', 'r'}, AllowedPos::BEG_MID, 91},
    {{'s', 'r'}, AllowedPos::BEG_MID, 92},
    {{'t', 'r'}, AllowedPos::BEG_MID, 93},

    {{'s', 's'}, AllowedPos::MID_END, 94},
    {{'s', 't'}, AllowedPos::EVERYWHERE, 95},
    {{'s', 't', 'r'}, AllowedPos::BEG_MID, 96},

    {{'q', 'u'}, AllowedPos::BEG_MID, 97},
};

template<size_t size>
SyllableSpec_t my_lower_bound(
        const std::array<SyllableSpec_t, size>& specs, 
        int8_t selector) {
    auto dummy_spec = SyllableSpec_t{{}, {}, selector};
    return *std::lower_bound(
            specs.begin(),
            specs.end(),
            dummy_spec,
            [](const SyllableSpec_t& spec, const SyllableSpec_t& dummy) {
                return spec.cumulative_weigth < dummy.cumulative_weigth;
            });
}

inline 
void Generate(char *out, int out_len, uint32_t seed)
{
    auto prng = std::minstd_rand(seed);
    auto vowel_distrib = std::uniform_int_distribution<int8_t>(
        0, vowels.back().cumulative_weigth);
    auto cons_distrib = std::uniform_int_distribution<int8_t>(
        0, consonants.back().cumulative_weigth);
    bool vowel_turn = (prng() & 1);

    int curr_len = 0;
    while (curr_len < out_len) {
        auto syllable_spec = SyllableSpec_t{}; 
        while (true) {
            if (vowel_turn) { 
                syllable_spec = my_lower_bound(vowels, vowel_distrib(prng));
            } else {
                syllable_spec = my_lower_bound(consonants, cons_distrib(prng));
            }
            if (curr_len == 0) {
                if (syllable_spec.allowed_pos & AllowedPos::BEGINING) { break; }
            } else if (curr_len + strlen(syllable_spec) == out_len) {
                if (syllable_spec.allowed_pos & AllowedPos::ENDING) { break; }
            } else if (curr_len + strlen(syllable_spec) < out_len) {
                if (syllable_spec.allowed_pos & AllowedPos::MIDDLE) { break; }
            }
        }
        strcpy(std::next(out, curr_len), syllable_spec.syllable.data());
        curr_len += strlen(syllable_spec);
        vowel_turn = !vowel_turn;
    }
}

} // namespace totro